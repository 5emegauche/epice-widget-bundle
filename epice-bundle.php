<?php

/*
Plugin Name: Epice Widgets Bundle Plugin
Description: Plugin Widgets Epice.
Version: 0.1
Author: Bertrand JAN
Author URI: 
License: GPL3
License URI:
Text Domain: epice-widget-text-domain
Domain Path: /languages
*/

define('EPICE_WIDGET_PREFIX','epice_widget');
define('EPICE_WIDGET_DOMAIN','epice-bundle');

function add_epice_widgets_collection($folders)
{
  $folders[] = plugin_dir_path(__FILE__).'widgets/';
  return $folders;
}
add_filter('siteorigin_widgets_widget_folders','add_epice_widgets_collection');

if(!function_exists(EPICE_WIDGET_PREFIX.'_load_textdomain'))
{
  function epice_widget_load_textdomain()
  {
    load_plugin_textdomain(EPICE_WIDGET_DOMAIN,false,dirname(plugin_basename(__FILE__)).'/languages');
  }
}
add_action('plugins_loaded',EPICE_WIDGET_PREFIX.'_load_textdomain');
