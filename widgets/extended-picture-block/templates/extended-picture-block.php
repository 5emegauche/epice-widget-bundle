<div class="extended-picture-block epice-block">
<?php //if(!empty(wp_kses_post($instance['title']))): ?>
<h3 class="epice-block-title epice-main-title"><?php echo wp_kses_post($instance['title']); ?></h3>
<?php //endif; ?>
<figure><?php echo wp_get_attachment_image(wp_kses_post($instance['picture']),'full'); ?></figure>

<div class="epice-block-text"><?php echo wp_kses_post($instance['legend']); ?></div>
</div>
