<?php

/*
Widget Name: Epice Bloc Social media
Description: Un bloc social media
Author: Me
Author URI: 
Widget URI: 
Video URI: 
*/

class Social_Block_Widget extends SiteOrigin_Widget
{
  function __construct()
  {
    //Here you can do any preparation required before calling the parent constructor, such as including additional files or initializing variables.

    //Call the parent constructor with the required arguments.
    parent::__construct(
        // The unique id for your widget.
        'social-block-widget',

        // The name of the widget for display purposes.
        __('Social block',EPICE_WIDGET_DOMAIN),

        // The $widget_options array, which is passed through to WP_Widget.
        // It has a couple of extras like the optional help URL, which should link to your sites help or support page.
        array(
          'description' => __('Social block description',EPICE_WIDGET_DOMAIN),
          'help'        => __('Social block help',EPICE_WIDGET_DOMAIN)
        ),

        //The $control_options array, which is passed through to WP_Widget
        array(
        ),

        //The $form_options array, which describes the form fields used to configure SiteOrigin widgets. We'll explain these in more detail later.
        array(
          'picture' => array(
            'type' => 'media',
            'label' => __('Choose a media thing',EPICE_WIDGET_DOMAIN),
            'choose' => __('Choose image',EPICE_WIDGET_DOMAIN),
            'update' => __('Set image',EPICE_WIDGET_DOMAIN),
            'library' => 'image'
          ),
          'title' => array(
            'type' => 'text',
            'label' => __('Social title',EPICE_WIDGET_DOMAIN),
            'default' => ''
          ),
          'name' => array(
            'type' => 'text',
            'label' => __('Social name',EPICE_WIDGET_DOMAIN),
            'default' => ''
          ),
          'hashtags' => array(
            'type' => 'text',
            'label' => __('Social hashtags',EPICE_WIDGET_DOMAIN),
            'default' => ''
          ),
          'link' => array(
            'type' => 'text',
            'label' => __('Social link',EPICE_WIDGET_DOMAIN),
            'default' => ''
          )
        ),

        //The $base_folder path string.
        plugin_dir_path(__FILE__)
    );
  }

  function get_template_name($instance)
  {
      return 'social-block';
  }

  function get_template_dir($instance)
  {
    return 'templates';
  }

  function get_style_name($instance)
  {
    return '';
  }
}

siteorigin_widget_register('social-block-widget', __FILE__, 'Social_Block_Widget');
