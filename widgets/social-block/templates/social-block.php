<div class="social-block epice-block">
<h3 class="epice-block-title epice-main-title"><?php echo wp_kses_post($instance['title']); ?></h3>
<figure><?php echo wp_get_attachment_image(wp_kses_post($instance['picture']),'full'); ?></figure>
<p class="epice-block-title"><?php echo wp_kses_post($instance['name']); ?></p>

<p class="epice-block-text"><?php echo wp_kses_post($instance['hashtags']); ?></p>
<p class="origin-widget-button"><a href="<?php echo wp_kses_post($instance['link']); ?>"><?php _e('Follow us',EPICE_WIDGET_DOMAIN); ?></a></p>
</div>
