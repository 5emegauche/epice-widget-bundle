<blockquote class="cite-block epice-block">
<?php echo wp_get_attachment_image(wp_kses_post($instance['picture']),'full'); ?>
<div class="epice-detail vertical-middle epice-block-text"><?php echo wp_kses_post($instance['cite']) ?></div>
</blockquote>
