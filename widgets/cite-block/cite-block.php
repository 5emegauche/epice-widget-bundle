<?php

/*
Widget Name: Epice Bloc citation
Description: Un bloc de citation
Author: Me
Author URI: 
Widget URI: 
Video URI: 
*/

class Cite_Block_Widget extends SiteOrigin_Widget
{
  function __construct()
  {
    //Here you can do any preparation required before calling the parent constructor, such as including additional files or initializing variables.

    //Call the parent constructor with the required arguments.
    parent::__construct(
        // The unique id for your widget.
        'cite-block-widget',

        // The name of the widget for display purposes.
        __('Cite block',EPICE_WIDGET_DOMAIN),

        // The $widget_options array, which is passed through to WP_Widget.
        // It has a couple of extras like the optional help URL, which should link to your sites help or support page.
        array(
          'description' => __('Cite block description',EPICE_WIDGET_DOMAIN),
          'help'        => __('Cite block help',EPICE_WIDGET_DOMAIN)
        ),

        //The $control_options array, which is passed through to WP_Widget
        array(
        ),

        //The $form_options array, which describes the form fields used to configure SiteOrigin widgets. We'll explain these in more detail later.
        array(
          'cite' => array(
            'type' => 'textarea',
            'label' => __('Cite content',EPICE_WIDGET_DOMAIN),
            'default' => '',
            'allow_html_formatting' => true,
            'rows' => 10
          ),
          'picture' => array(
            'type' => 'media',
            'label' => __('Choose a media thing',EPICE_WIDGET_DOMAIN),
            'choose' => __('Choose image',EPICE_WIDGET_DOMAIN),
            'update' => __('Set image',EPICE_WIDGET_DOMAIN),
            'library' => 'image'
          )
        ),

        //The $base_folder path string.
        plugin_dir_path(__FILE__)
    );
  }

  function get_template_name($instance)
  {
      return 'cite-block';
  }

  function get_template_dir($instance)
  {
    return 'templates';
  }

  function get_style_name($instance)
  {
    return '';
  }
}

siteorigin_widget_register('cite-block-widget',__FILE__,'Cite_Block_Widget');
