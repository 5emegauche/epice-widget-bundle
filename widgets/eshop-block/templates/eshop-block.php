<?php

$currencySymbol = '€';

$locale = get_locale();
if($locale == 'ja')
{
  $currencySymbol = '¥';
}

?>
<div class="eshop-block epice-block">
<figure><?php echo wp_get_attachment_image(wp_kses_post($instance['picture']),'full'); ?></figure>
<h3 class="epice-block-title"><?php echo wp_kses_post($instance['title']); ?></h3>
<p class="epice-block-price"><?php echo wp_kses_post($instance['price']); ?>&#160;<?php echo $currencySymbol; ?></p>

<div class="epice-block-text"><?php echo wp_kses_post($instance['description']); ?></div>

<p class="origin-widget-button"><a href="<?php echo wp_kses_post($instance['link']); ?>" target="_blank"><?php _e('Buy',EPICE_WIDGET_DOMAIN); ?></a></p>

</div>
