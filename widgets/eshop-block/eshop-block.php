<?php

/*
Widget Name: Epice Bloc eShop
Description: Un bloc eShop
Author: Me
Author URI: 
Widget URI: 
Video URI: 
*/

class EShop_Block_Widget extends SiteOrigin_Widget
{
  function __construct()
  {
    //Here you can do any preparation required before calling the parent constructor, such as including additional files or initializing variables.

    //Call the parent constructor with the required arguments.
    parent::__construct(
        // The unique id for your widget.
        'eshop-block-widget',

        // The name of the widget for display purposes.
        __('EShop block',EPICE_WIDGET_DOMAIN),

        // The $widget_options array, which is passed through to WP_Widget.
        // It has a couple of extras like the optional help URL, which should link to your sites help or support page.
        array(
          'description' => __('EShop block description',EPICE_WIDGET_DOMAIN),
          'help'        => __('EShop block help',EPICE_WIDGET_DOMAIN)
        ),

        //The $control_options array, which is passed through to WP_Widget
        array(
        ),

        //The $form_options array, which describes the form fields used to configure SiteOrigin widgets. We'll explain these in more detail later.
        array(
          'picture' => array(
            'type' => 'media',
            'label' => __('Choose a media thing',EPICE_WIDGET_DOMAIN),
            'choose' => __('Choose image',EPICE_WIDGET_DOMAIN),
            'update' => __('Set image',EPICE_WIDGET_DOMAIN),
            'library' => 'image'
          ),
          'title' => array(
            'type' => 'text',
            'label' => __('Product name',EPICE_WIDGET_DOMAIN),
            'default' => ''
          ),
          'price' => array(
            'type' => 'number',
            'label' => __('Product price',EPICE_WIDGET_DOMAIN),
            'default' => ''
          ),
          'description' => array(
            'type' => 'textarea',
            'label' => __('Description content',EPICE_WIDGET_DOMAIN),
            'default' => '',
            'allow_html_formatting' => true,
            'rows' => 3
          ),
          'link' => array(
            'type' => 'text',
            'label' => __('Product link',EPICE_WIDGET_DOMAIN),
            'default' => ''
          )
        ),

        //The $base_folder path string.
        plugin_dir_path(__FILE__)
    );
  }

  function get_template_name($instance)
  {
      return 'eshop-block';
  }

  function get_template_dir($instance)
  {
    return 'templates';
  }

  function get_style_name($instance)
  {
    return '';
  }
}

siteorigin_widget_register('eshop-block-widget', __FILE__, 'EShop_Block_Widget');
