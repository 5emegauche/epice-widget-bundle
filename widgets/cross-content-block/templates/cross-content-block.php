
<?php

$crossId = wp_kses_post($instance['selected_post']);
$crossContent = get_post($crossId);

$crossCategories = wp_get_post_categories($crossId);

$catAr = array();
foreach($crossCategories as $cat)
{
  $currentCat = get_category($cat);
  $link = get_category_link($currentCat->cat_ID);

  $catAr[] = array(
    'name' => $currentCat->name,
    'link' => $link
  );
}

$postLink = get_permalink($crossId);

if(!empty($crossContent)) :
?>
<div class="cross-content-block epice-block">
<?php if(has_post_thumbnail($crossId)): ?>
<?php echo get_the_post_thumbnail($crossId,'full'); ?>
<?php endif; ?>

  <div class="epice-detail vertical-middle">
  <?php if(count($catAr) > 0): ?>
  <ul class="categories-list">
  <?php foreach($catAr as $catV): ?>
  <li><a href="<?php echo $catV['link']; ?>"><?php echo $catV['name']; ?></a></li>
  <?php endforeach; ?>
  </ul>
  <?php endif; ?>
  <h3 class="epice-block-title"><?php echo $crossContent->post_title; ?></h3>
  <p class="origin-widget-button"><a href="<?php echo $postLink; ?>"><?php _e('Discover',EPICE_WIDGET_DOMAIN); ?></a></p>
  </div>
</div>
<?php endif; ?>

