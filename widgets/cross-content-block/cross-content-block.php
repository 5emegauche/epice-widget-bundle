<?php

/*
Widget Name: Epice Bloc Cross content
Description: Un bloc cross content
Author: Me
Author URI: 
Widget URI: 
Video URI: 
*/

class Cross_Content_Block_Widget extends SiteOrigin_Widget
{
  function __construct()
  {
    //Here you can do any preparation required before calling the parent constructor, such as including additional files or initializing variables.

    $argsPost = array(
    'post_type' => 'post'
    );
    
    $loopPost = new WP_Query($argsPost);

    $allPosts = $loopPost->posts;

    $postsAr = array();

    foreach($allPosts as $vP)
    {
      $postsAr[$vP->ID] = $vP->post_title;
    }

    //Call the parent constructor with the required arguments.
    parent::__construct(
        // The unique id for your widget.
        'cross-content-block-widget',

        // The name of the widget for display purposes.
        __('Cross Content block',EPICE_WIDGET_DOMAIN),

        // The $widget_options array, which is passed through to WP_Widget.
        // It has a couple of extras like the optional help URL, which should link to your sites help or support page.
        array(
          'description' => __('Cross Content block description',EPICE_WIDGET_DOMAIN),
          'help'        => __('Cross Content block help',EPICE_WIDGET_DOMAIN)
        ),

        //The $control_options array, which is passed through to WP_Widget
        array(
        ),

        //The $form_options array, which describes the form fields used to configure SiteOrigin widgets. We'll explain these in more detail later.
        array(
          'selected_post' => array(
            'type' => 'select',
            'label' => __( 'Choose a thing from a long list of things',EPICE_WIDGET_DOMAIN),
            'options' => $postsAr
          )
        ),

        //The $base_folder path string.
        plugin_dir_path(__FILE__)
    );
  }

  function get_template_name($instance)
  {
      return 'cross-content-block';
  }

  function get_template_dir($instance)
  {
    return 'templates';
  }

  function get_style_name($instance)
  {
    return '';
  }
}

siteorigin_widget_register('cross-content-block-widget', __FILE__, 'Cross_Content_Block_Widget');
